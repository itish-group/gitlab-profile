# ITish Group

Welcome to the ITish GitLab group! This space is dedicated to the "ITish" blog site project. 

## Demo

Click **<a href="https://itish.live" target="_blank">here</a>** to open ITish blog site

_Below are the repositories associated with this group:_

## 1. [ITish](https://gitlab.com/itish-group/ITish)
**Description:**
The `ITish` repository houses the Flask blog site, designed for publishing insightful posts on IT topics. The project utilizes Flask's powerful features and customizable templates for an engaging blogging experience.

## 2. [FastAPI](https://gitlab.com/itish-group/FastAPI)
**Description:**
The `FastAPI` repository is dedicated to the API component of the "ITish" blog site. Leveraging FastAPI, it provides a robust backend to support the functionality of the blog, allowing for efficient data handling and processing.

## 3. [Vue-frontend](https://gitlab.com/itish-group/Vue-frontend)
**Description:**
The `Vue-frontend` repository hosts the frontend component with Vue.js for the "ITish" API (FastAPI). This frontend is designed to provide a seamless and dynamic user interface for interacting with the blog's data.

## 4. [gRPC](https://gitlab.com/itish-group/gRPC)
**Description:**
The `gRPC` repository focuses on implementing gRPC for the "ITish" blog site. This communication protocol enhances the efficiency of data exchange between different components of the project.

> This README.md generated with assistance from ChatGPT
